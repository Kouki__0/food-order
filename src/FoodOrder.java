import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodOrder {
    private JPanel root;
    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JButton button4;
    private JButton button5;
    private JButton button6;
    private JTextPane textPane1;
    private JButton button7;
    private JLabel total;

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrder");
        frame.setContentPane(new FoodOrder().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    int all=0;
    void order (String food, int price){
        int confirmation = JOptionPane.showConfirmDialog (
                null ,
                "Would you like to order "+food+" ?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if (confirmation==0){
            JOptionPane.showMessageDialog (null ,"Thank you for ordering "+food+" ! It will be served as soon as possible." );
            String currentText = textPane1.getText();
            textPane1.setText(currentText + food+" " + price + " yen" + "\n");
            all += price;
            total.setText("Total "+all+" yen");

        }
    }

    public FoodOrder() {
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Apple", 200);
            }
        });
        button1.setIcon(new ImageIcon( this.getClass().getResource("apple.jpg")
        ));
        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order ("Banana",100);
            }
        });
        button2.setIcon(new ImageIcon( this.getClass().getResource("banana.jpg")
        ));
        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order ("Kiwi",300);
            }
        });
        button3.setIcon(new ImageIcon( this.getClass().getResource("kiwi.jpg")
        ));
        button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order ("Lemon",200);
            }
        });
        button4.setIcon(new ImageIcon( this.getClass().getResource("lemon.jpg")
        ));
        button5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order ("Mango",400);
            }
        });
        button5.setIcon(new ImageIcon( this.getClass().getResource("mango.jpg")
        ));
        button6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order ("Melon",900);
            }
        });
        button6.setIcon(new ImageIcon( this.getClass().getResource("melon.jpg")
        ));
        button7.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog (
                        null ,
                        "Would you like to checkout ?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);

                if (confirmation==0){
                    JOptionPane.showMessageDialog (null ,"Thank you. The total price is "+all+" yen" );
                    textPane1.setText("");
                    total.setText("Total     0 yen");
                }
            };
        });
    };
}
